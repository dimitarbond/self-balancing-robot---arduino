#include <MPU6050_tockn.h>
#include <Wire.h>
#include <PID_v1.h>

MPU6050 mpu6050(Wire);

#define enA 10
#define in1 9
#define in2 8
#define in3 5
#define in4 4
#define enB 3

int pwmOutput = 0;
double agoly= 0;
int brzina = 50;
float offset = 10;
float granica = 2;

double Setpoint, Input, Output;
double Kp=3, Ki=0, Kd=0;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);
int dirFlag=0;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  Input = double(mpu6050.getAngleY());
  Setpoint = offset;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);

}
void nazad(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);

  analogWrite(enA, brzina);
  analogWrite(enB, brzina);
}
void napred(){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);

  analogWrite(enA, brzina);
  analogWrite(enB, brzina);
}

void loop() {

  mpu6050.update();
  Input=mpu6050.getAngleY();
  if(Input>0)
  {
    Input=-Input;
    dirFlag=1;
  }
  else
    dirFlag=0;
  myPID.Compute();
  Output=int(Output);
  Serial.print("Input: ");
  Serial.print(Input);
  Serial.print("  Output: ");
  Serial.print(Output);
  Serial.print("  Dirrection: ");
  Serial.print(dirFlag);

  if(dirFlag==1){
   if(Output > 235)
      Output = 255;
    else
      Output = Output;

    // Output = map(Output, 0, 100, 20, 255);
    brzina=Output;
    napred();
  }
  if(dirFlag==0){
    if(Output > 235)
      Output = 255;
    else
      Output = Output;

    // Output = map(Output, 0, 100, 0, 255);
    brzina= Output;
    nazad();
  }
  Serial.print("  Brzina: ");
  Serial.println(brzina);
  /*if(agoly>(offset-granica) && agoly<(offset+granica)){

  analogWrite(enA, 0);
  analogWrite(enB, 0);

  }*/
}
