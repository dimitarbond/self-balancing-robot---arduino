#include <MPU6050_tockn.h>
#include <Wire.h>

MPU6050 mpu6050(Wire);

#define enA 10
#define in1 8
#define in2 9
#define in3 5
#define in4 4
#define enB 3

float offset = -8;
float speed;
int dirFlag;

//PID constants
double kp = 1;
double ki = 0;
double kd = 0;

unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
double input, output, setPoint;
double cumError, rateError;

void setup() {

  Serial.begin(9600);
  Wire.begin();
  mpu6050.begin();
  //mpu6050.calcGyroOffsets(true);
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  setPoint = offset;

}

double computePID(double inp){
        currentTime = millis();                //get current time
        elapsedTime = (double)(currentTime - previousTime);        //compute time elapsed from previous computation

        error = setPoint - inp;                                // determine error
        cumError += error * elapsedTime;                // compute integral
        rateError = (error - lastError)/elapsedTime;   // compute derivative

        double out = kp*error + ki*cumError + kd*rateError;                //PID output

        lastError = error;                                //remember current error
        previousTime = currentTime;                        //remember current time

        return out;                                        //have function return the PID output
}

void backward(int speed){
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);

  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void forward(int speed){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);

  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void loop(){
       mpu6050.update();
       input = double(mpu6050.getAngleY());
       if(input>0)
         {
           input=-input;
           dirFlag=1;
         }
        else
          dirFlag=0;

        output = int(computePID(input));
        Serial.print("Input: ");
        Serial.print(input - offset);
        Serial.print("  Output: ");
        Serial.print(output);
        Serial.print("  Dirrection: ");
        Serial.print(dirFlag);

        if(dirFlag==1){
          if(output > 235 || output < -235)
             output = 255;
          else if (output < 10 && output >-10)
             output = abs(output);
          else if ((output > 10 && output < 40) || (output < -10 && output > -40))
             output = 50;
          else
             output = abs(output);

           // output = map(output, 0, 100, 20, 255);
          speed = output;
          forward(speed);
         }
        if(dirFlag==0){
          if(output > 235 && output < -235)
             output = 255;
          else if (output < 10 && output >-10)
             output = abs(output);
          else if ((output > 10 && output < 40) || (output < -10 && output > -40))
             output = 50;
          else
             output = abs(output);

           // output = map(output, 0, 100, 0, 255);
           speed = output;
           backward(speed);
         }
        Serial.print("  Speed: ");
        Serial.println(speed);

         /*if(agoly>(offset-granica) && agoly<(offset+granica)){

         analogWrite(enA, 0);
         analogWrite(enB, 0);

         }*/
}
