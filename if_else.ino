#include <MPU6050_tockn.h>
#include <Wire.h>

MPU6050 mpu6050(Wire);

#define enA 10
#define in1 9
#define in2 8
#define in3 5
#define in4 4
#define enB 3

int pwmOutput = 0;
double agoly= 0;
int brzina = 50;
float offset = -8.5;
float granica = 2;

void setup() {

  Serial.begin(9600);
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  agoly = double(mpu6050.getAngleY());
}

void nazad(int brzina){

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);

  analogWrite(enA, brzina);
  analogWrite(enB, brzina);

}
void napred(int brzina){

  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);

  analogWrite(enA, brzina);
  analogWrite(enB, brzina);

}

void loop() {

  mpu6050.update();
  agoly=mpu6050.getAngleY();
  Serial.print(agoly);
  if(agoly>granica+offset)
  {
    napred(brzina);
    if(agoly>granica+offset+5)
    napred(brzina+10);
    if(agoly>granica+offset+10)
    napred(brzina+20);
  }
  if(agoly<-granica+offset)
  {
    nazad(brzina);
    if(agoly<-granica+offset-5)
    nazad(brzina+10);
    if(agoly<-granica+offset-10)
    nazad(brzina+20);
  }
  else if(agoly<granica+offset && agoly>-granica+offset){

    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);
  }

}
